# Limehome Booking App #

This application is created by Tolga Bilbey, and it is given by Limehome as a full stack developer coding challange.

It consists of 3 parts. 

1. Frontend Implementation (Vue)
2. Backend Implementation (Django)
3. Hosted Deployment (Heroku)

## Dependencies

1. Python 3
2. Vue-cli

## Usage

1. Create a virtualenv for backend project. (I am using pyenv and plugins)
2. Install dependencies for backend and frontend using
```bash
pip install -r requirements.txt
npm install
```
3. Create a build for frontend using
```bash
npm run build
```
4. Create an .env file that has DATABASE_URL variable in it like
DATABASE_URL=sqlite:///db.sqlite3
5. Migrate database and collect static files using
```bash
python manage.py migrate
python manage.py collectstatic
```
6. Run the application
```bash
python manage.py runserver
```

## Testing

Django server has test implementations.

```bash
python manage.py test
```
or you can use coverage like
```bash
coverage run --source="." manage.py test
```
and see the report using
```bash
coverage report
```

## Hosted Environment

Heroku used to deploy this application. You can reach it via https://limehome-tolga.herokuapp.com/.

### Demo account informations (to check admin page):

username: demo

password: demo

