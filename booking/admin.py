from django.contrib import admin
from booking.models import Booking


class BookingAdmin(admin.ModelAdmin):
    list_filter = ["check_in_date", "check_out_date", "here_place_id"]
    list_display = [
        "code", "here_place_id", "check_in_date",
        "check_out_date", "name", "surname"]
    search_filter = ["here_place_id", "name", "surname", "code"]


admin.site.register(Booking, BookingAdmin)
