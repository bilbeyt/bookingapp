import uuid
from datetime import date
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError


class Booking(models.Model):
    code = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    check_in_date = models.DateField()
    check_out_date = models.DateField()
    here_place_id = models.CharField(max_length=200)

    def __str__(self):
        return str(self.code)


@receiver(pre_save, sender=Booking)
def check_model(instance, sender, *args, **kwargs):
    if instance.check_in_date < date.today():
        raise ValidationError(
            'Check in date must be later than now.')
    if instance.check_out_date <= instance.check_in_date:
        raise ValidationError(
            'Check out date must be later than check in date.')
