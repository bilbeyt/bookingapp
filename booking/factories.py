from factory.django import DjangoModelFactory
from datetime import date
from faker import Factory

from booking.models import Booking

faker = Factory.create()


class BookingFactory(DjangoModelFactory):
    class Meta:
        model = Booking

    name = faker.first_name()
    surname = faker.last_name()
    here_place_id = faker.uuid4()
    check_in_date = date.today()
    check_out_date = faker.future_date()