from datetime import date, timedelta

from django.test import TestCase
from django.core.exceptions import ValidationError
from faker import Faker

from booking.models import Booking


faker = Faker()


class BookingTestCase(TestCase):
    def test_valid_booking(self):
        checkin = date.today()
        checkout = checkin + timedelta(days=3)
        booking = Booking.objects.create(
            name=faker.first_name(),
            surname=faker.last_name(),
            here_place_id=faker.word(),
            check_in_date=checkin,
            check_out_date=checkout
        )
        self.assertEqual(booking.check_in_date, checkin)
        self.assertEqual(booking.check_out_date, checkout)
        self.assertEqual(str(booking), str(booking.code))

    def test_wrong_checkin_date(self):
        checkin = date.today() - timedelta(days=1)
        checkout = checkin + timedelta(days=3)
        params = {
            "name": faker.first_name(),
            "surname": faker.last_name(),
            "here_place_id": faker.word(),
            "check_in_date": checkin,
            "check_out_date": checkout
        }
        with self.assertRaises(ValidationError):
            Booking.objects.create(**params)

    def test_wrong_checkout_date(self):
        checkin = date.today()
        checkout = checkin - timedelta(days=1)
        params = {
            "name": faker.first_name(),
            "surname": faker.last_name(),
            "here_place_id": faker.word(),
            "check_in_date": checkin,
            "check_out_date": checkout
        }
        with self.assertRaises(ValidationError):
            Booking.objects.create(**params)
