from django_filters import FilterSet
from rest_framework.viewsets import ModelViewSet

from api.serializers import BookingSerializer
from booking.models import Booking


class BookingFilter(FilterSet):
    class Meta:
        model = Booking
        exclude = []


class BookingViewSet(ModelViewSet):
    queryset = Booking.objects.all().order_by("id")
    serializer_class = BookingSerializer
    filter_class = BookingFilter
