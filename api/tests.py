import json
from datetime import date

from django.test import TestCase
from faker import Faker
from rest_framework.test import APIClient
from booking.factories import BookingFactory


faker = Faker()


class BookingApiTestCase(TestCase):
    def setUp(self):
        self.bookings = BookingFactory.create_batch(20)
        self.client = APIClient()

    def test_booking_list(self):
        response = self.client.get(
            "/api/bookings/", content_type="application/json")
        self.assertEqual(response.renderer_context["response"].status_code, 200)
        self.assertEqual(response.data["count"], len(self.bookings))

    def test_booking_detail(self):
        booking = self.bookings[0]
        response = self.client.get(
            f"/api/bookings/{booking.id}/", content_type="application/json")
        self.assertEqual(response.renderer_context["response"].status_code, 200)
        self.assertEqual(response.data["id"], booking.id)

    def test_booking_filter_by_place_id(self):
        booking = self.bookings[0]
        response = self.client.get(
            f"/api/bookings/?here_place_id={booking.here_place_id}",
            content_type="application/json")
        self.assertEqual(response.renderer_context["response"].status_code, 200)
        self.assertEqual(response.data["count"], len(self.bookings))

    def test_create_booking(self):

        params = {
            "name": faker.first_name(),
            "surname": faker.last_name(),
            "here_place_id": faker.word(),
            "check_in_date": date.today().isoformat(),
            "check_out_date": faker.future_date().isoformat()
        }

        response = self.client.post(
            "/api/bookings/",
            json.dumps(params),
            content_type="application/json"
        )

        self.assertEqual(response.renderer_context["response"].status_code, 201)
        self.assertEqual(response.data["name"], params["name"])

    def test_update_booking(self):
        booking = self.bookings[0]
        params = {
            "name": faker.first_name(),
            "surname": faker.last_name(),
            "here_place_id": faker.word(),
            "check_in_date": date.today().isoformat(),
            "check_out_date": faker.future_date().isoformat()
        }

        response = self.client.put(
            f"/api/bookings/{booking.id}/",
            json.dumps(params),
            content_type="application/json"
        )

        self.assertEqual(response.renderer_context["response"].status_code, 200)
        self.assertEqual(response.data["name"], params["name"])

    def test_delete_booking(self):
        booking = self.bookings[0]

        response = self.client.delete(
            f"/api/bookings/{booking.id}/",
            content_type="application/json"
        )

        self.assertEqual(response.renderer_context["response"].status_code, 204)
